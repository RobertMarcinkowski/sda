package com.example;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.domain.ImieINazwisko;
import com.domain.Movie;
import com.domain.MovieComment;
import com.domain.MovieRating;
import com.domain.Producer;
import com.repository.MovieCommentRepository;
import com.repository.MovieRatingRepository;
import com.repository.MovieRepository;
import com.repository.ProducerRepository;

@RestController
public class MovieController {

	@Autowired
	MovieRepository movieRepo;

	@Autowired
	ProducerRepository producerRepository;

	@Autowired
	MovieRatingRepository movieRatingRepository;

	@Autowired
	MovieCommentRepository movieCommentRepository;

	@GetMapping(path = "/getMovie")
	public List<String> getMovie() {
		ImieINazwisko imieINazwisko = new ImieINazwisko();
		imieINazwisko.setImie("aa");
		imieINazwisko.setNazwisko("bb");

		Producer producer = new Producer();
		producer.setNazwaStudia("nn");
		producer.setImieINazwisko(imieINazwisko);

		Movie movie = new Movie();
		movie.setLength(170);
		movie.setTitle("mm");
		movie.setYear("2017");
		movie.setProducer(producer);

		movieRepo.add(movie);

		List<Producer> list = producerRepository.findAll("Super studio");
		for (Producer p : list) {
			for (Movie m : p.getMovies()) {
				System.out.println(m.getTitle());
			}
		}

		Optional<Producer> p = producerRepository.findProducer(new Long(4));
		Optional<Producer> p2 = producerRepository.findProducer(new Long(4));
		p.ifPresent(x -> System.out.println("nasze studio: " + x.getNazwaStudia()));

		if (p.isPresent()) {
			Producer prodZBazy = p.get();
			prodZBazy.setNazwaStudia("nowe studio11");
			producerRepository.merge(prodZBazy);
			Producer prodZBazy2 = p.get();
			prodZBazy2.setNazwaStudia("nowe studio 2!11");
			producerRepository.merge(prodZBazy2);
		}
		if (p2.isPresent()) {
			Producer prodZBazy = p2.get();
			prodZBazy.setNazwaStudia("ekstra nowe studio11");
			producerRepository.merge(prodZBazy);
		}
		System.out.println("////////////////////////");
		List<Movie> listShort = movieRepo.findShortMovies(115);
		for (Movie movie2 : listShort) {
			System.out.println(movie2.getTitle());
		}

		List<Movie> shortMovie = movieRepo.findShortMovies(111);
		shortMovie.stream().forEach(x -> System.out.println("short movie: " + x.getTitle()));

		if (movie.getTitle() != null) {
			System.out.println(movie.getTitle().toUpperCase());
		}
		// ==
		String title = Optional.ofNullable(movie.getTitle().toUpperCase()).orElse("");
		System.out.println("======================");
		List<Movie> top10Movie = movieRepo.findTopTenShortMoviesByProducerName(111, "Jakub");
		top10Movie.stream().forEach(x -> System.out.println("top movie: " + x.getId() + " " + x.getTitle()));

		System.out.println("======================");
		List<Movie> titleMovie = movieRepo.findMoviesByTitle("bc");
		titleMovie.stream().forEach(x -> System.out.println("title movie: " + x.getId() + " " + x.getTitle()));

		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^");
		Optional<Producer> producerByName = producerRepository.findByFirstNameAndLastName("jaN", "koWAlSkI");
		producerByName.ifPresent(x -> System.out.println("znaleziony producent: " + x.getId() + " - "
				+ x.getImieINazwisko().getImie() + " " + x.getImieINazwisko().getNazwisko()));
		// String imiee =
		// producerByName.map(x->x.getImieINazwisko().getImie()).orElse("Brak");
		// Producer producerNowy = producerByName.orElse(new Producer());
		System.out.println("^^&&^^&&^^&&^^&&^^&&^^&&^^&&^^");
		// List<Movie> allMovieWithCriteria =
		// movieRepo.findAllWithCriteria("Dobry film 2");

		// List<Movie> allMovieWithCriteria = movieRepo.findAllWithCriteria("D",
		// 2001);
		List<Movie> allMovieWithCriteria = movieRepo.findAllWithCriteria(null, null, null);
		List<Movie> MovieWith2Criteria = movieRepo.findByTitleAndYear(null, null);

		allMovieWithCriteria.stream()
				.forEach(x -> System.out.println("movie: " + x.getTitle() + " " + x.getYear() + " " + x.getLength()));
		System.out.println("--**---***---***---**--");
		MovieWith2Criteria.stream().forEach(x -> System.out.println("movie: " + x.getTitle() + " " + x.getYear()));

		List<Movie> movieListToRating = movieRepo.findAll();
		movieListToRating.stream().forEach(x -> addRating(x));

		List<Movie> movieWithAvg = movieRepo.findByAvgMin(4);
		// movieWithAvg.stream().forEach(x -> System.out.println("movie: " +
		// x.getId() + " - " + x.getTitle() + " avg "
		// + x.getMovieRating().stream().mapToDouble(a ->
		// a.getRate()).average().getAsDouble()));
		movieWithAvg.stream()
				.forEach(x -> System.out.println("movie: " + x.getId() + " - " + x.getTitle() + " avg " + x.getAvg()));
		System.out.println("..............................");
		List<Movie> movieAllTitleAndAvg = movieRepo.findAllTitleAndAvg();
		movieAllTitleAndAvg.stream()
				.forEach(x -> System.out.println("movie: " + x.getId() + " - " + x.getTitle() + " avg " + x.getAvg()));

		List<String> movieWithAvgx = movieRepo.findAllTitleAndAvg().stream().map(x -> x.getTitle() + " " + x.getAvg())
				.collect(Collectors.toList());
		System.out.println("______________________________________");
		System.out.println("||||||||||||||||||||||||||||||||||||||");
		System.out.println("______________________________________");

		Optional<Movie> filmDoKomentarz = movieRepo.findAll().stream().findAny();
		filmDoKomentarz.ifPresent(x -> addComment(x));

		return movieWithAvgx;
	}

	private void addRating(Movie m) {
		Random r = new Random();
		MovieRating mr = new MovieRating();
		mr.setMovie(m);
		mr.setEmail("test@sda.pl");
		mr.setRate(r.nextInt(10));
		movieRatingRepository.persist(mr);

	}

	private void addComment(Movie m) {
		Random r = new Random();
		MovieComment mc = new MovieComment();
		mc.setComment("super film !! polecam :)");
		mc.setMovie(m);
		movieCommentRepository.persist(mc);
	}

}
