package com.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.domain.Movie;

@Repository
@Transactional
public class MovieRepositoryImpl implements MovieRepository {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Movie> findAll() {
		try {
			String sql = "SELECT e FROM Movie e";
			Query query = em.createQuery(sql);
			return query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public List<Movie> findShortMovies(int length) {
		try {
			String hql = "SELECT e FROM Movie e WHERE e.length < :length";
			Query query = em.createQuery(hql);
			query.setParameter("length", length);
			return query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public List<Movie> findTopTenShortMoviesByProducerName(int length, String firstName) {
		try {
			String hql = "SELECT e FROM Movie e WHERE e.length < :xyz" + " AND e.producer.imieINazwisko.imie = :imie";
			Query query = em.createQuery(hql);
			query.setParameter("imie", firstName);
			query.setParameter("xyz", length);
			query.setMaxResults(10);
			query.setFirstResult(1);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public List<Movie> findMoviesByTitle(String title) {
		try {
			String hql = "SELECT e FROM Movie e WHERE e.title LIKE :title";
			Query query = em.createQuery(hql);
			query.setParameter("title", "%" + title + "%");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public Boolean delete(Movie movie) {
		try {
			em.remove(movie);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public Movie add(Movie movie) {
		try {
			em.persist(movie);
			em.clear();
			return movie;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean merge(Movie movie) {
		try {
			em.merge(movie);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Movie> findByTitleAndYear(String nazwa, Integer year) {
		return findAllWithCriteria(nazwa, year, null);
	}

	@Override
	public List<Movie> findAllWithCriteria(String nazwa, Integer year, Integer length) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Movie> query = cb.createQuery(Movie.class);

			Root<Movie> m = query.from(Movie.class);
			query.select(m);

			Predicate titlePredicate = cb.like(m.get("title"), "%" + nazwa + "%");
			Predicate yearPredicate = cb.equal(m.get("year"), year);
			Predicate lengthPredicate = cb.equal(m.get("length"), length);

			List<Predicate> list = new ArrayList<>();
			if (nazwa != null) {
				list.add(titlePredicate);
			}
			if (year != null) {
				list.add(yearPredicate);
			}
			if (length != null) {
				list.add(lengthPredicate);
			}

			// query.where(cb.like(m.get("title"), "%" + nazwa + "%"));
			// query.where(cb.and(titlePredicate, yearPredicate));
			query.where(cb.or(list.toArray(new Predicate[list.size()])));
			return em.createQuery(query).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public List<Movie> findByAvgMin(int rate) {
		try {
			String hql = "SELECT m FROM Movie m LEFT JOIN m.movieRating mr GROUP BY mr.movie HAVING AVG(mr.rate) > :rate";
			Query query = em.createQuery(hql);
			query.setParameter("rate", new Double(rate));
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public List<Movie> findAllTitleAndAvg() {
		try {
			String hql = "SELECT new Movie(m.title, AVG(mr.rate) ) FROM Movie m LEFT JOIN m.movieRating mr GROUP BY mr.movie ORDER BY AVG(mr.rate) DESC";
			Query query = em.createQuery(hql);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}
}
