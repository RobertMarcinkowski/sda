package com.repository;

import java.util.List;
import java.util.Optional;

import com.domain.Producer;

public interface ProducerRepository {

	// List<Producer> findAll();

	Producer merge(Producer producer);

	void persist(Producer producer);

	Optional<Producer> findProducer(Long id);

	List<Producer> findAll(String nazwaStudia);

	Optional<Producer> findByFirstNameAndLastName(String firstName, String lastName);

}
