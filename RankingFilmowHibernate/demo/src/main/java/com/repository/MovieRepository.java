package com.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.domain.Movie;

@Component
public interface MovieRepository {

	List<Movie> findAll();

	Boolean delete(Movie movie);

	Movie add(Movie movie);

	Boolean merge(Movie movie);

	List<Movie> findShortMovies(int length);

	List<Movie> findTopTenShortMoviesByProducerName(int length, String firstName);

	List<Movie> findMoviesByTitle(String string);

	// List<Movie> findAllWithCriteria(String nazwa);

	// List<Movie> findAllWithCriteria(String nazwa, int year);

	// List<Movie> findAllWithCriteria(String nazwa, Integer year);

	List<Movie> findAllWithCriteria(String nazwa, Integer year, Integer length);

	List<Movie> findByTitleAndYear(String nazwa, Integer year);

	List<Movie> findByAvgMin(int rate);

	List<Movie> findAllTitleAndAvg();

}
