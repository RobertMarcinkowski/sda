package pl.sdacademy;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import pl.sdacademy.controller.MainController;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub

		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/MainView.fxml"));
		StackPane stackPane = loader.load();

		// Controller in FXML or add below
		//
		MainController controller = new MainController();
		controller.setStage(primaryStage);
		loader.setController(controller);

		Scene scene = new Scene(stackPane);
		String cssPath = this.getClass().getResource("/styles.css").toExternalForm();
		scene.getStylesheets().add(cssPath);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Second application with FXML");
		primaryStage.show();

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
