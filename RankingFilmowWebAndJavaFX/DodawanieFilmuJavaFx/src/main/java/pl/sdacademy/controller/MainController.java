package pl.sdacademy.controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MainController {

	private FXMLLoader loader;

	public FXMLLoader getLoader() {
		return loader;
	}

	@FXML
	private StackPane mainStackPane;

	private Stage stage;

	public Stage getStage() {
		return stage;
	}

	@FXML
	void initialize() {
		initMenuView();
	}

	public void initMenuView() {
		loadView("MenuView");
		MenuController controller = loader.getController();
		controller.setMainController(this);
	}

	public void setView(Pane pane) {
		mainStackPane.getChildren().clear();
		mainStackPane.getChildren().add(pane);
	}

	public void loadView(String name) {
		loader = new FXMLLoader(this.getClass().getResource("/" + name + ".fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setView(pane);
	}

	public void setStage(Stage stage) {
		// TODO Auto-generated method stub
		this.stage = stage;
	}
}
