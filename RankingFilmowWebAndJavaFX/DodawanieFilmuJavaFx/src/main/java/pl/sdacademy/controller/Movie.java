package pl.sdacademy.elements;

public class Movie {

	private String tytul;
	private String producent;
	private String rok;
	private String dlugosc;

	public Movie(String tytul, String producent, String rok, String dlugosc) {
		super();
		this.tytul = tytul;
		this.producent = producent;
		this.rok = rok;
		this.dlugosc = dlugosc;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getProducent() {
		return producent;
	}

	public void setProducent(String producent) {
		this.producent = producent;
	}

	public String getRok() {
		return rok;
	}

	public void setRok(String rok) {
		this.rok = rok;
	}

	public String getDlugosc() {
		return dlugosc;
	}

	public void setDlugosc(String dlugosc) {
		this.dlugosc = dlugosc;
	}

	@Override
	public String toString() {
		return "Movie [tytul=" + tytul + ", producent=" + producent + ", rok=" + rok + ", dlugosc=" + dlugosc + "]";
	}

}
