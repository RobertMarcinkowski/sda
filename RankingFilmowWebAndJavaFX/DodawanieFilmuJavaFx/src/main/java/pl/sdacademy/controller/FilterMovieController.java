package pl.sdacademy.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import webservice.WebserviceService;

public class FilterMovieController {

	private MainController mainController;

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}

	@FXML
	private URL location;

	@FXML
	private Button backButton;

	@FXML
	private Font x1;

	@FXML
	private Color x2;

	@FXML
	private Button send;

	@FXML
	private TextField filterByYear;

	@FXML
	void onActionBack(ActionEvent event) {
		mainController.initMenuView();
	}

	@FXML
	void onActionSend(ActionEvent event) {
		int rok = Integer.parseInt(filterByYear.getText());
		try {
			// sendPost(rok);
			findByYear(rok);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void initialize() {
		// ObservableList<Integer> years = FXCollections.observableArrayList();
		// years.add(1);
		// years.add(2);
		// length = new ComboBox<Integer>(years);
	}

	public void sendPost(int rok) throws Exception {

		String url = "http://localhost:8080/RankingFilmow/";
		URL obj = new URL(url);
		java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();

		String USER_AGENT = "Mozilla/5.0";

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "findMovie?rokOd=" + rok;

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

	public void findByYear(int year) {
		WebserviceService web = new WebserviceService();
		List<webservice.Movie> list = web.getWebservicePort().findByYear(year);
		list.stream().forEach(x -> System.out.println(x.getTytul()));
	}

}
