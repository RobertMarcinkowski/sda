package pl.sdacademy.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MenuController {

	private MainController mainController;

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private Button option1;

	@FXML
	private Button option2;

	@FXML
	private Button option3;

	@FXML
	void initialize() {

	}

	@FXML
	public void openOption1() {
		mainController.loadView("AddMovieApplication");
		AddMovieController controller = mainController.getLoader().getController();
		controller.setMainController(mainController);
	}

	@FXML
	public void openOption2() {
		mainController.loadView("FilterMovieApplication");
		FilterMovieController controller = mainController.getLoader().getController();
		controller.setMainController(mainController);
	}

	@FXML
	public void openOption3() {
		mainController.loadView("EditMovieApplication");
		EditMovieController controller = mainController.getLoader().getController();
		controller.setMainController(mainController);
	}

}
