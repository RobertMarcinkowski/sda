package pl.sdacademy.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import pl.sdacademy.elements.Movie;
import webservice.WebserviceService;

public class AddMovieController {

	private MainController mainController;

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField length;

	@FXML
	private TextField producer;

	@FXML
	private Button send;

	@FXML
	private TextField title;

	@FXML
	private TextField year;

	@FXML
	private Button backButton;

	@FXML
	void onActionBack(ActionEvent event) {
		mainController.initMenuView();
	}

	@FXML
	void onActionSend(ActionEvent event) {
		Movie movie = new Movie(title.getText(), producer.getText(), year.getText(), length.getText());
		System.out.println(movie);
		try {
			addMovie(movie);
			// sendPost(movie);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void initialize() {
		// ObservableList<Integer> years = FXCollections.observableArrayList();
		// years.add(1);
		// years.add(2);
		// length = new ComboBox<Integer>(years);
	}

	public void sendPost(Movie movie) throws Exception {

		String url = "http://localhost:8080/RankingFilmow/newMovie";
		URL obj = new URL(url);
		java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();

		String USER_AGENT = "Mozilla/5.0";

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		String urlParameters = "tytul=" + movie.getTytul() + "&rok=" + movie.getRok() + "&producent="
				+ movie.getProducent() + "&dlugosc=" + movie.getDlugosc() + "";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

	public void addMovie(Movie movie) {
		// tworzymy obiekt Movie z pakietu webservice
		webservice.Movie movieDoWyslania = MovieBuilder(movie);

		WebserviceService webserviceService = new WebserviceService();
		// wykonujemy metode addMovie
		webserviceService.getWebservicePort().addMovie(movieDoWyslania);

		List<webservice.Movie> movieList = webserviceService.getWebservicePort().getMovies();
		movieList.stream().forEach(x -> System.out.println(x.getTytul()));
	}

	private webservice.Movie MovieBuilder(Movie movie) {
		webservice.Movie movieDoWyslania = new webservice.Movie();
		movieDoWyslania.setDlugosc(Integer.valueOf(movie.getDlugosc()));
		movieDoWyslania.setProducent(movie.getProducent());
		movieDoWyslania.setRok(movie.getRok());
		movieDoWyslania.setTytul(movie.getTytul());
		return movieDoWyslania;
	}

}
