
package webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for movie complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="movie">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acotrs" type="{http://webservice/}actor" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dlugosc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="producent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rok" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tytul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "movie", propOrder = {
    "acotrs",
    "dlugosc",
    "producent",
    "rok",
    "tytul"
})
public class Movie {

    @XmlElement(nillable = true)
    protected List<Actor> acotrs;
    protected int dlugosc;
    protected String producent;
    protected String rok;
    protected String tytul;

    /**
     * Gets the value of the acotrs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acotrs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcotrs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Actor }
     * 
     * 
     */
    public List<Actor> getAcotrs() {
        if (acotrs == null) {
            acotrs = new ArrayList<Actor>();
        }
        return this.acotrs;
    }

    /**
     * Gets the value of the dlugosc property.
     * 
     */
    public int getDlugosc() {
        return dlugosc;
    }

    /**
     * Sets the value of the dlugosc property.
     * 
     */
    public void setDlugosc(int value) {
        this.dlugosc = value;
    }

    /**
     * Gets the value of the producent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducent() {
        return producent;
    }

    /**
     * Sets the value of the producent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducent(String value) {
        this.producent = value;
    }

    /**
     * Gets the value of the rok property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRok() {
        return rok;
    }

    /**
     * Sets the value of the rok property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRok(String value) {
        this.rok = value;
    }

    /**
     * Gets the value of the tytul property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTytul() {
        return tytul;
    }

    /**
     * Sets the value of the tytul property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTytul(String value) {
        this.tytul = value;
    }

}
