
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EditActor_QNAME = new QName("http://webservice/", "editActor");
    private final static QName _GetMovies_QNAME = new QName("http://webservice/", "getMovies");
    private final static QName _EditActorResponse_QNAME = new QName("http://webservice/", "editActorResponse");
    private final static QName _GetMoviesResponse_QNAME = new QName("http://webservice/", "getMoviesResponse");
    private final static QName _AddMovie_QNAME = new QName("http://webservice/", "addMovie");
    private final static QName _AddMovieResponse_QNAME = new QName("http://webservice/", "addMovieResponse");
    private final static QName _AddActor_QNAME = new QName("http://webservice/", "addActor");
    private final static QName _ShowActors_QNAME = new QName("http://webservice/", "showActors");
    private final static QName _ShowActorsResponse_QNAME = new QName("http://webservice/", "showActorsResponse");
    private final static QName _AddActorResponse_QNAME = new QName("http://webservice/", "addActorResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EditActor }
     * 
     */
    public EditActor createEditActor() {
        return new EditActor();
    }

    /**
     * Create an instance of {@link GetMovies }
     * 
     */
    public GetMovies createGetMovies() {
        return new GetMovies();
    }

    /**
     * Create an instance of {@link EditActorResponse }
     * 
     */
    public EditActorResponse createEditActorResponse() {
        return new EditActorResponse();
    }

    /**
     * Create an instance of {@link GetMoviesResponse }
     * 
     */
    public GetMoviesResponse createGetMoviesResponse() {
        return new GetMoviesResponse();
    }

    /**
     * Create an instance of {@link AddMovie }
     * 
     */
    public AddMovie createAddMovie() {
        return new AddMovie();
    }

    /**
     * Create an instance of {@link AddMovieResponse }
     * 
     */
    public AddMovieResponse createAddMovieResponse() {
        return new AddMovieResponse();
    }

    /**
     * Create an instance of {@link AddActor }
     * 
     */
    public AddActor createAddActor() {
        return new AddActor();
    }

    /**
     * Create an instance of {@link ShowActors }
     * 
     */
    public ShowActors createShowActors() {
        return new ShowActors();
    }

    /**
     * Create an instance of {@link ShowActorsResponse }
     * 
     */
    public ShowActorsResponse createShowActorsResponse() {
        return new ShowActorsResponse();
    }

    /**
     * Create an instance of {@link AddActorResponse }
     * 
     */
    public AddActorResponse createAddActorResponse() {
        return new AddActorResponse();
    }

    /**
     * Create an instance of {@link Actor }
     * 
     */
    public Actor createActor() {
        return new Actor();
    }

    /**
     * Create an instance of {@link Movie }
     * 
     */
    public Movie createMovie() {
        return new Movie();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditActor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "editActor")
    public JAXBElement<EditActor> createEditActor(EditActor value) {
        return new JAXBElement<EditActor>(_EditActor_QNAME, EditActor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMovies }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getMovies")
    public JAXBElement<GetMovies> createGetMovies(GetMovies value) {
        return new JAXBElement<GetMovies>(_GetMovies_QNAME, GetMovies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditActorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "editActorResponse")
    public JAXBElement<EditActorResponse> createEditActorResponse(EditActorResponse value) {
        return new JAXBElement<EditActorResponse>(_EditActorResponse_QNAME, EditActorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMoviesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getMoviesResponse")
    public JAXBElement<GetMoviesResponse> createGetMoviesResponse(GetMoviesResponse value) {
        return new JAXBElement<GetMoviesResponse>(_GetMoviesResponse_QNAME, GetMoviesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMovie }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "addMovie")
    public JAXBElement<AddMovie> createAddMovie(AddMovie value) {
        return new JAXBElement<AddMovie>(_AddMovie_QNAME, AddMovie.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMovieResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "addMovieResponse")
    public JAXBElement<AddMovieResponse> createAddMovieResponse(AddMovieResponse value) {
        return new JAXBElement<AddMovieResponse>(_AddMovieResponse_QNAME, AddMovieResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddActor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "addActor")
    public JAXBElement<AddActor> createAddActor(AddActor value) {
        return new JAXBElement<AddActor>(_AddActor_QNAME, AddActor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowActors }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "showActors")
    public JAXBElement<ShowActors> createShowActors(ShowActors value) {
        return new JAXBElement<ShowActors>(_ShowActors_QNAME, ShowActors.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowActorsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "showActorsResponse")
    public JAXBElement<ShowActorsResponse> createShowActorsResponse(ShowActorsResponse value) {
        return new JAXBElement<ShowActorsResponse>(_ShowActorsResponse_QNAME, ShowActorsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddActorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "addActorResponse")
    public JAXBElement<AddActorResponse> createAddActorResponse(AddActorResponse value) {
        return new JAXBElement<AddActorResponse>(_AddActorResponse_QNAME, AddActorResponse.class, null, value);
    }

}
