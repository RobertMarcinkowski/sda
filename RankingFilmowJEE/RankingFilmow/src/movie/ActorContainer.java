package movie;

import java.util.List;

public interface ActorContainer {

	List<Aktor> getActors();

}
