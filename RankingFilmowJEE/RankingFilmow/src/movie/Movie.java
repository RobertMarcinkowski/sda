package movie;

import java.util.ArrayList;
import java.util.List;

public class Movie {
	private String tytul;
	private String producent;
	private String rok;
	private int dlugosc;
	private List<Aktor> actors = new ArrayList<>();

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getProducent() {
		return producent;
	}

	public void setProducent(String producent) {
		this.producent = producent;
	}

	public String getRok() {
		return rok;
	}

	public void setRok(String rok) {
		this.rok = rok;
	}

	public int getDlugosc() {
		return dlugosc;
	}

	public void setDlugosc(int dlugosc) {
		this.dlugosc = dlugosc;
	}

	public List<Aktor> getActors() {
		return actors;
	}

	public void setActors(List<Aktor> actors) {
		this.actors = actors;
	}

	public Movie(String tytul, String producent, String rok, int dlugosc, List<Aktor> actors) {
		super();
		this.tytul = tytul;
		this.producent = producent;
		this.rok = rok;
		this.dlugosc = dlugosc;
		this.actors = actors;
	}

	public Movie() {
	}

	@Override
	public String toString() {
		return "Movie [tytul=" + tytul + ", producent=" + producent + ", rok=" + rok + ", dlugosc=" + dlugosc
				+ ", actors=" + actors + "]";
	}

}
