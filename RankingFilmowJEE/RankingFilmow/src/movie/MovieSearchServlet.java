package movie;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("search2")
public class MovieSearchServlet extends HttpServlet {

	@Inject
	MovieContainer movieContainer;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException {
		ServletOutputStream out = response.getOutputStream();
		for (Movie m : movieContainer.getMovies()) {
			if (m.getTytul().equals(req.getParameter("tytul"))) {
				out.print("znalazlem " + m);
			}
		}
		out.print("hello");
		out.close();
	}

}
