package movie;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MovieListServlet extends HttpServlet {

	@Inject
	MovieContainer movieContainer;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException {
		response.setContentType("text/html; charset=UTF-8");
		Calendar c = Calendar.getInstance();
		c.set(2016, 11, 1);
		Aktor benedictCumberbatch = new Aktor("Benedict", "Cumberbatch", 40);
		List<Aktor> actors = new ArrayList<>();
		actors.add(benedictCumberbatch);
		Movie doktorStrange = new Movie("Doktor Strange", "Marvel Studios", "2016-11-01", 180, actors);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		ServletOutputStream out = response.getOutputStream();
		out.print("<html>");
		out.print("<body>");
		out.print("<table>" + "<thead>" + "<th>Tytul</th>" + "<th>Rok</th>" + "</thead><tbody>");

		for (Movie m : movieContainer.getMovies()) {
			out.print("<tr><th>" + m.getTytul() + "</th><th>" + sdf.format(m.getRok()) + "</th></tr>");
		}

		out.print("</table></body></html>");
		out.close();
	}
}
