package movie;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ActorContainerImpl implements ActorContainer {
	List<Aktor> actors = new ArrayList<>();

	@Override
	public List<Aktor> getActors() {
		return actors;
	}

	public void setActors(List<Aktor> actors) {
		this.actors = actors;
	}

}
