package time;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

public class Time {
	public void time() {

		// Instant otherInstant = Instant.now();
		// Instant i = Instant.now();
		// ZonedDateTime terazPolska = i.atZone(ZoneId.of("Europe/Warsaw"));
		// i.isAfter(otherInstant);
		// i.isBefore(otherInstant);
		//
		// Instant dateOne = Instant.parse("1970-11-10T10:42:55.034Z");
		// Instant dateTwo = Instant.parse("2016-11-10T10:42:55.034Z");
		// System.out.println(dateOne.compareTo(dateTwo));
		//
		// ZonedDateTime pierwszyNas =
		// terazPolska.with(TemporalAdjusters.firstDayOfNextMonth());
		// ZonedDateTime najblizszyPon =
		// terazPolska.with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY))
		// .truncatedTo(ChronoUnit.DAYS);
		// System.out.println(pierwszyNas);
		// System.out.println(najblizszyPon);

		////////////////////////////////////////////////////
		Instant i = Instant.now();
		// Instant i = Instant.parse("2012-06-06T10:11:00.000Z");
		ZonedDateTime terazPolska = i.atZone(ZoneId.of("Europe/Warsaw"));

		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm");

		String text2 = terazPolska.format(formatter2);
		// ZonedDateTime date = ZonedDateTime.parse(text, formatter);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd");
		formatter.withZone(ZoneId.of("Europe/Warsaw"));
		String text = terazPolska.format(formatter);
		LocalDate dateLocal = LocalDate.parse(text, formatter);
		ZonedDateTime zdt = dateLocal.atStartOfDay(ZoneId.systemDefault());
		System.out.println(dateLocal);

		System.out.println(text);

		//////////////////////////////////////////////////////
		LocalDate czas1 = LocalDate.parse("2017 03 25", formatter).with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY));
		System.out.println(czas1 + " " + czas1.getDayOfWeek());
		System.out.println(Period.between(LocalDate.now(), czas1));

		System.out.println(ChronoUnit.DAYS.between(dateLocal, czas1));

		ZonedDateTime t1 = LocalDate.parse(terazPolska.format(formatter), formatter)
				.atStartOfDay(ZoneId.systemDefault());
		ZonedDateTime t2 = LocalDate.parse(czas1.format(formatter), formatter).atStartOfDay(ZoneId.systemDefault());
		System.out.println(t1 + " " + t2);
		Duration duration = Duration.between(t1, t2);
		duration.toDays();
		System.out.println(duration.toDays());

		//////////////////////////////////////////////////////

		LocalDate czas2 = LocalDate.parse("2016 10 26", formatter);
		System.out.println(ChronoUnit.DAYS.between(czas2, dateLocal));

	}

}