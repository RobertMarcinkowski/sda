package webservice;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import movie.ActorContainer;
import movie.Aktor;
import movie.Movie;
import movie.MovieContainer;

@WebService
public class MovieWebservice {

	@Inject
	MovieContainer movieContainer;
	@Inject
	ActorContainer actorContainer;

	private String message = new String("Hello, ");

	public void Hello() {

	}

	@WebMethod
	public String sayHello(String name) {
		return message + name + ".";
	}

	@WebMethod
	public List<Movie> listMovies() {
		return movieContainer.getMovies();
	}

	@WebMethod
	public String addMovie(Movie movie) {
		movieContainer.getMovies().add(movie);
		return "Otrzymalem : " + movie.getTytul() + " Mam " + movieContainer.getMovies().size() + " filmow ";
	}

	@WebMethod
	public String addActor(Aktor actor) {
		actorContainer.getActors().add(actor);
		return "Otrzymalem : " + actor.getImie() + " " + actor.getNazwisko() + " Mam "
				+ actorContainer.getActors().size() + " aktor�w ";
	}

	@WebMethod
	public List<Aktor> listActors() {
		return actorContainer.getActors();
	}

	@WebMethod
	public String editActor(Aktor actor) {
		Optional<Aktor> actorToEdit = actorContainer.getActors().stream().filter(x -> x.getId().equals(actor.getId()))
				.findAny();
		if (actorToEdit.isPresent()) {
			Aktor a = actorToEdit.get();
			a.setImie(actor.getImie());
			a.setNazwisko(actor.getNazwisko());
			a.setWiek(actor.getWiek());

			actorContainer.getActors().remove(actorToEdit.get());
			actorContainer.getActors().add(a);
			return "Udalo sie!";
		} else {
			return "Brak aktora";
		}

	}

}
