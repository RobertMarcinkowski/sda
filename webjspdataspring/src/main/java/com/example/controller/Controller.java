package com.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by RENT on 2017-02-28.
 */
@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping("/hello")
    @ResponseBody
    public String helloWorld(){
        return "Hello world! SDA";
    }

    @RequestMapping("/hellojsp")
    public ModelAndView helloWorldJsp(ModelAndView modelAndView) {
        modelAndView.setViewName("/hello");
        return modelAndView;
    }
}
