package pl.sda.java20122016.quiz;

import java.util.List;
import java.util.Scanner;

public class QuizInterfejs {
	public Integer menu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("1. Start");
		System.out.println("2. Wyniki");
		System.out.println("3. Koniec");
		Integer opcja = scanner.nextInt();
		return opcja;
	}

	public void przedStartem() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Zaraz zacznie się gra");
		scanner.nextLine();
	}

	public boolean wyswietlPytanie(Pytanie pytanie) {
		System.out.println(pytanie.getPytanie());
		System.out.println("________________________");
		List<String> odpowiedzi = pytanie.getOdpowiedzi();
		odpowiedzi.forEach(e -> System.out.println(e));
		System.out.println("________________________");
		System.out.println("Podaj odpowiedź: ");
		Scanner scanner = new Scanner(System.in);
		String odpowiedz = scanner.nextLine();
		if (odpowiedz.equals(pytanie.getPoprawnaOdpowiedz())) {
			return true;
		} else {
			return false;
		}

	}

	public void poprawnaOdpowiedz() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Poprawna odpowiedz");
		scanner.nextLine();
	}

	public void niepoprawnaOdpowiedz() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Niepoprawna odpowiedz");
		scanner.nextLine();
	}

}
