package pl.sda.java20122016.quiz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Quiz {
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("quiz.txt");
		QuizInterfejs quizInterfejs = new QuizInterfejs();

		QuizOperacjeZPliku quizOperacjeZPliku = new QuizOperacjeZPliku();
		List<Pytanie> listaPytan = quizOperacjeZPliku.pytaniaZPlikuJakoLista(file);
		int licznik = 0;

		Integer opcja = quizInterfejs.menu();
		switch (opcja) {
		case 1:
			quizInterfejs.przedStartem();
			for (int i = 0; i < listaPytan.size(); i++) {
				boolean odp = quizInterfejs.wyswietlPytanie(listaPytan.get(i));
				if (odp) {
					quizInterfejs.poprawnaOdpowiedz();
					licznik++;
				} else {
					quizInterfejs.niepoprawnaOdpowiedz();
				}
			}

			// TODO przeniesc wyswietlenie wynikow do klasy QuizInterfejs
			System.out.println("Twoj wynik to " + licznik + "/" + listaPytan.size());
			break;

		default:
			break;
		}
		quizInterfejs.przedStartem();

	}
}
