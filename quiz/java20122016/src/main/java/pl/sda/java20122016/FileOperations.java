package pl.sda.java20122016;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FileOperations {

	public static void main(String[] args) throws IOException {
		// File file = new File("test.txt");
		// readFromFile(file);
		// writeToFile(file, "My text");
		// List<String> namesList = namesFromFilesToList(file);
		// printList(namesList);
		// Map<String, Integer> map = dataFromFileToMap(file);
		// printMap(map);
		File file = new File("katalog.txt");
		File file2 = new File("katalog2.txt");
		Map<String, User> map = usersFromFileToMap(Arrays.asList(file, file2));
		printUsers(map);

	}

	private static void printList(List<String> names) {
		names.forEach(e -> System.out.println(e));
	}

	private static void printMap(Map<String, Integer> map) {
		map.forEach((k, v) -> System.out.println("| Key: " + k + "\t| Value: " + v + " |"));
	}

	private static void printUsers(Map<String, User> map) {
		map.forEach((k, v) -> System.out.println("| Key: " + k + "\t| Value: " + v + "\t|"));
	}

	public static void readFromFile(File file) throws FileNotFoundException {
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {
			System.out.println(scanner.nextLine());
		}
	}

	public static void writeToFile(File file, String message) throws IOException {
		try (FileWriter fw = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			out.println(message);
		}
	}

	public static List<String> namesFromFilesToList(File file) throws FileNotFoundException {
		List<String> namesList = new ArrayList<>();
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {
			namesList.add(scanner.nextLine());
		}
		return namesList;
	}

	public static Map<String, Integer> dataFromFileToMap(File file) throws FileNotFoundException {
		Map<String, Integer> map = new HashMap<>();
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {

			String line = scanner.nextLine();
			String[] split = line.split("_");
			map.put(split[0], new Integer(split[1]));
		}
		return map;
	}

	public static Map<String, User> usersFromFileToMap(File file) throws FileNotFoundException {
		Map<String, User> usersMap = new HashMap<>();
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {

			String line = scanner.nextLine();
			String[] split = line.split(" ");
			User user = new User();
			user.setFirstName(split[1]);
			user.setLastName(split[2]);
			user.setPhineNumber(split[3]);
			usersMap.put(split[0], user);
		}
		return usersMap;
	}

	public static Map<String, User> usersFromFileToMap(List<File> files) throws FileNotFoundException {
		Map<String, User> usersMap = new HashMap<>();
		files.forEach(e -> {
			try {
				usersMap.putAll(usersFromFileToMap(e));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		});
		return usersMap;

	}

}
