package pl.sda.java20122016;

public class User {
	private String firstName;
	private String lastName;
	private String phoneNumber;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhineNumber() {
		return phoneNumber;
	}

	public void setPhineNumber(String phineNumber) {
		this.phoneNumber = phineNumber;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", phoneNumber=" + phoneNumber + "]";
	}

}
