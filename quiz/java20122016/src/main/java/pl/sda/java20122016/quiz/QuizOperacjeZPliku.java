package pl.sda.java20122016.quiz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuizOperacjeZPliku {
	public List<Pytanie> pytaniaZPlikuJakoLista(File file) throws FileNotFoundException {
		List<Pytanie> listaPytan = new ArrayList<>();
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {
			Pytanie pytanie = new Pytanie();
			pytanie.setPytanie(scanner.nextLine());
			List<String> odpowiedzi = new ArrayList<>();
			for (int i = 0; i < 4; i++) {
				String odpowiedz = scanner.nextLine();
				odpowiedzi.add(odpowiedz);
			}
			pytanie.setOdpowiedzi(odpowiedzi);
			pytanie.setPoprawnaOdpowiedz(scanner.nextLine());
			listaPytan.add(pytanie);
		}
		return listaPytan;

	}

}
