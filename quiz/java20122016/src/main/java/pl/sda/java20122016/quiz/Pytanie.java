package pl.sda.java20122016.quiz;

import java.util.List;

public class Pytanie {
	private String pytanie;
	private List<String> odpowiedzi;
	private String poprawnaOdpowiedz;

	public String getPytanie() {
		return pytanie;
	}

	public void setPytanie(String pytanie) {
		this.pytanie = pytanie;
	}

	public List<String> getOdpowiedzi() {
		return odpowiedzi;
	}

	public void setOdpowiedzi(List<String> odpowiedzi) {
		this.odpowiedzi = odpowiedzi;
	}

	public String getPoprawnaOdpowiedz() {
		return poprawnaOdpowiedz;
	}

	public void setPoprawnaOdpowiedz(String poprawnaOdpowiedz) {
		this.poprawnaOdpowiedz = poprawnaOdpowiedz;
	}

}
