<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; UTF-8" %>
<html>
<head>
    <jsp:include page="common/stylesAndScripts.jsp"/>
</head>
<body>
    <div class="container"/>
        <div class="row">
            <form:form method="post" action="/register" modelAttribute="user" class="form-horizontal">
                <div class="form-group">
                    <form:label path="username" class="col-sm-2 control-label" for="inputUsername">username</form:label>
                    <div class="col-sm-10">
                        <form:input path="username" type="text" class="form-control" id="inputUsername" placeholder="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="password" for="inputPassword" class="col-sm-2 control-label">Password</form:label>
                    <div class="col-sm-10">
                        <form:input path="password" type="password" class="form-control" id="inputPassword" placeholder="Password"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="firstName" for="inputfirstName" class="col-sm-2 control-label">firstName</form:label>
                    <div class="col-sm-10">
                        <form:input path="firstName" type="text" class="form-control" id="inputfirstName" placeholder="firstName"/>
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="familyName" for="inputfamilyName" class="col-sm-2 control-label">familyName</form:label>
                    <div class="col-sm-10">
                        <form:input path="familyName" type="text" class="form-control" id="inputfamilyName" placeholder="familyName"/>
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="eMail" for="inputeMail" class="col-sm-2 control-label">eMail</form:label>
                    <div class="col-sm-10">
                        <form:input path="eMail" type="text" class="form-control" id="inputeMail" placeholder="eMail"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" value="Submit">Sign in</button>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</body>
</html>