package com.example.controller;

import com.example.model.Person;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by RENT on 2017-03-04.
 */
@Controller
public class PersonController {

    private static final Logger LOGGER = Logger.getLogger(PersonController.class);

    @Value("${test.value}")
    private static String testValue;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/person")
    public ModelAndView getPerson(@RequestParam("id") String id, ModelAndView modelAndView) {
        Person person = null;
        try {
            person = restTemplate.getForObject("http://localhost:8080/resource/person/" + id, Person.class);
        } catch (HttpStatusCodeException exception) {
            LOGGER.error(exception.getMessage());
        }
        String test = testValue;
        modelAndView.addObject("id", id);
        modelAndView.addObject("person", person);
        modelAndView.setViewName("/new");
        return modelAndView;
    }
}
